package main

func main() {
	//此处作为服务器巡检
	/*
		1.硬件配置信息

		1.1机器型号
		[root@gip 20230810]# dmidecode | grep "Product Name"
		    Product Name: Standard PC (i440FX + PIIX, 1996)
		1.2CPU
		[root@gip 20230810]# cat /proc/cpuinfo | grep name | cut -f2 -d: | uniq -c
		  8  QEMU Virtual CPU version 2.0.
		1.3内存
		[root@gip 20230810]# free -m
		1.4磁盘
		[root@gip 20230810]# df -h
		1.5 网卡
		[root@gip 20230810]# ifconfig -a
		1.6操作系统
		[root@gip 20230810]# uname -a

		2. 性能检查

		2.1 CPU占用率  top
		cpu每个线程占用率小于80%

		2.2 CPU负载 uptinme
		[root@gip 20230810]# uptime
		 14:49:50 up 31 min,  2 users,  load average: 0.01, 0.04, 0.05
		CPU负载应小于10
		1分钟内的平均负载是0.01
		5分钟内的平均负载是0.04
		15分钟内的平均负载是0.05

		2.3内存占用率
		[kaito@db160 ~]$ free -m 内存使用率小于80%

		2.4 内存swap使用率 free -m 小于10

		2.5 磁盘使用率 小于80% df-ah

		2.6磁盘负载 iostat xkd 1 util应在10% 以内


		2.7 系统开放端口 无关端口要关闭 netstat -an | grep LISTEN

		2.8 网卡流量 iftop 网卡流量小于宽带的50%

		2.9 进程检查 ps aux | more 关闭占用资源的无用进程


	*/
}

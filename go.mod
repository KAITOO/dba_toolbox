module dba_toolbox

go 1.20

require (
	github.com/go-sql-driver/mysql v1.7.0
	github.com/gookit/color v1.5.3
)

require (
	github.com/dlclark/regexp2 v1.10.0 // indirect
	github.com/phpdave11/gofpdi v1.0.14-0.20211212211723-1f10f9844311 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/signintech/gopdf v0.18.0 // indirect
	github.com/tiechui1994/gopdf v0.0.0-20220723131034-4b11a58b280a
	golang.org/x/image v0.11.0 // indirect
)

require (
	github.com/chzyer/readline v1.5.1
	github.com/xo/terminfo v0.0.0-20210125001918-ca9a967f8778 // indirect
	golang.org/x/sys v0.6.0 // indirect
)
